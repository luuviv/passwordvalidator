package password;

import java.util.Random;

/*
 * @author Vivian Luu, 991558427
 * 
 * 
 * This class will be created and developed using TDD
 * 
 * */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	private static int MIN_NUM_DIGITS = 2; 
	
	
	public static boolean hasValidCaseChars(String password) {
		
		//test for uppercase then lowercase
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
		
	}
	
	
	public static boolean isValidLength(String password) {
		
		if(password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasEnoughDigits(String password) {
		
		return password !=null && password.matches("([^0-9]*[0-9]){2}.*") && password.trim().length() >= MIN_NUM_DIGITS;
	}
	
	protected static String generateRandomPassword() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		int size = (int) (rnd.nextFloat() * 16);
		while(salt.length() < size) { //length of random string
			int index = (int)(rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}
	
	public static void main(String args[]) {
		System.out.println("Password Validator 1.0");
		
		String password = generateRandomPassword();
		
		System.out.println("Password generated for testing" + password);
		System.out.println("Is valid password? : " + (isValidLength(password) && hasEnoughDigits(password)));
		
		
	}
}

